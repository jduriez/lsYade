/*************************************************************************
*  Copyright (C) 2004 by Olivier Galizzi                                 *
*  olivier.galizzi@imag.fr                                               *
*                                                                        *
*  This program is free software; it is licensed under the terms of the  *
*  GNU General Public License v2 or later. See file LICENSE for details. *
*************************************************************************/

#pragma once

#include <lib/base/Math.hpp>
#include <string>
#include <vector>

namespace yade { // Cannot have #include directive inside.


class MarchingCube {
	/// ATTRIBUTES
private:
	vector<Vector3r> triangles; // {triangles[i],triangles[i+1],triangles[i+2]} = vertices of i-th triangle
	vector<Vector3r> normals;
	int nbTriangles;
	int sizeX, sizeY, sizeZ; // number of grid vertices along the 3 axes
	Real isoValue;
	vector<vector<vector<Vector3r>>> positions; // the gridpoints of a Regular Grid TODO: replace with a (i,j,k) function
	static const int edgeArray[256];
	static const int triTable[256][16];
	Vector3r         aNormal;
	/// PRIVATE METHOD
	/** triangulate cell (x,y,z) **/
	void polygonize(const vector<vector<vector<Real>>>& scalarField, int x, int y, int z);
	/** compute normals of the triangles previously found with polygonize in cell (x,y,z)
		@param n : indice of the first triangle to process
	**/
	void computeNormal(const vector<vector<vector<Real>>>& scalarField, int x, int y, int z, int offset, int triangleNum);
	/** interpolate coordinates of point vect (that is on isosurface) from coordinates of points vect1 et vect2 **/
	void interpolate(const Vector3r& vect1, const Vector3r& vect2, Real val1, Real val2, Vector3r& vect);
	/** Same as interpolate but in 1D **/
	Real interpolateValue(Real val1, Real val2, Real val_cible1, Real val_cible2);
	/** Compute normal to vertice or triangle inside cell (x,y,z) **/
	const Vector3r& computeNormalX(const vector<vector<vector<Real>>>& scalarField, int x, int y, int z);
	const Vector3r& computeNormalY(const vector<vector<vector<Real>>>& scalarField, int x, int y, int z);
	const Vector3r& computeNormalZ(const vector<vector<vector<Real>>>& scalarField, int x, int y, int z);
	/// CONSTRUCTOR/DESTRUCTOR
public:
	MarchingCube();
	~MarchingCube();
	/// PUBLIC METHODS
	const vector<Vector3r>& getTriangles() { return triangles; }
	const vector<Vector3r>& getNormals() { return normals; }
	int getNbTriangles() { return nbTriangles; }
	void computeTriangulation(const vector<vector<vector<Real>>>& scalarField, Real iso);
	void init(int sx, int sy, int sz, const Vector3r& min, const Vector3r& max); // init in particular the "positions" of the underlying regular grid
	void resizeScalarField(vector<vector<vector<Real>>>& scalarField, int sx, int sy, int sz); // scalarField is sx*sy*sz in size after execution, including new 0 if previous size was smaller
};

}; // namespace yade
