=======
LS-YADE
=======

This C++/Python repository is companion of the manuscript `A Level Set-Discrete Element Method in YADE for numerical, micro-scale, geomechanics with refined grain shapes <https://www.sciencedirect.com/science/article/pii/S0098300421002247>`_ published in *Computers & Geosciences* by Jerome Duriez and Cedric Galusinski.

**Source code and licence**

Level Set-Discrete Element Method (LS-DEM) is inserted into the 2020.01a version of the YADE platform (whose project is at https://gitlab.com/yade-dev/trunk). The present code has been developed by Jerome Duriez, first author of the manuscript and is released under the GNU General Public License v2.
 
**Installation**

Please execute bash script install.sh for installation

**Documentation**

Documentation can be obtained after launching a `make doc` command from the build folder, as well as from Python interface, please see the manuscript.
General documentation of the YADE platform (without the present LS-DEM implementation) can be found at https://yade-dem.org/doc/

See also the "Computer code availability" of the manuscript.

**Future development and maintenance**

Further development is expected to take place in `YADE project <https://gitlab.com/yade-dev/trunk>`_ itself.
