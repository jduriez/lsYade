# -*- encoding=utf-8 -*-
# jerome.duriez@inrae.fr
# A single contact between two superquadric ellipsoids described with Polyhedra, for the timing analysis of section 5.3, in comparison with ../levelSet/seContact.py

nN = 100 # number of vertices
from yade import ymport,plot,timing
import time
execfile('../levelSet/ramFP.py')

O.timingEnabled=True

#########################
### Engine definition ###
#########################

O.dt = 1.
nItMax = 150
O.engines=[ForceResetter()
           ,InsertionSortCollider([Bo1_Polyhedra_Aabb()],verletDist = 0) # verletDist could be sthg else than 0 here
           ,InteractionLoop(
               [Ig2_Polyhedra_Polyhedra_PolyhedraGeom()],
               [Ip2_PolyhedraMat_PolyhedraMat_PolyhedraPhys()],
               [Law2_PolyhedraGeom_PolyhedraPhys_Volumetric()]
               ,label='iLoop')
           ,NewtonIntegrator()
           ,PyRunner(command='saveThings()',iterPeriod=1)
          ]
def saveThings():
    plot.addData(it = O.iter,tILoop = iLoop.execTime/1000.# tILoop now in us
    )

#########################
### Bodies definition ###
#########################

# Body 0 shall be at the origin, axis-aligned, and the biggest one
O.materials.append(PolyhedraMat())

epsVal = numpy.array([[1.4,1.2] # ~ the ellipsoid
                     ,[0.4,1.6] # ~ the almond
                      ])
rVal = numpy.array([[0.8,1.2,1.6]
                   ,[0.7,1.2,0.9]
                    ])

lVert = []
prec = 10
for shape in range(2):
    print('Shape',shape,'with',nN,'nodes: LS-computations')
    lsBod = levelSetBody("superellipsoid",extents = rVal[shape],epsilons=epsVal[shape],spacing = min(rVal[shape])/prec,nNodes=nN,nodesPath=2)
    if len(lsBod.shape.boundNodes)!=nN:
        raise BaseException('PROBLEM: got only',len(lsBod.shape.boundNodes),'vs',nN,'expected')
    lVert.append(lsBod.shape.boundNodes)

# some orientation and position for the almond:
iniOri = Quaternion((0.04478760831548503352,0.9377162807771838304,0.344502898256000889),2.58242771884218536)
iniCentr = Vector3(1.092,0.989,0.833)
vertGlobal = [iniCentr+iniOri*vert for vert in lVert[1]]

ramFPini = ramFP()
tStart = time.time()
O.bodies.append(polyhedron(lVert[0],wire=False,fixed=True))
O.bodies.append(polyhedron(vertGlobal,wire=False,fixed=True))
tEnd = time.time()
print(len(O.bodies),'polyhedra for a total of',2*nN,'vertices generated in',tEnd-tStart,'s, and',ramFP() - ramFPini,'MB of RAM')

###########
### Run ###
###########

view = yade.qt.View()
view.sceneRadius = 1
O.run(nItMax,True)
if not O.interactions.has(0,1):
    raise BaseException('PROBLEM: no interaction !')
else:
    if not O.interactions[0,1].isReal:
        raise BaseException('PROBLEM: no real interaction !')
    volCbc = O.interactions[0,1].geom.penetrationVolume**(1/3.)
    print('Interaction obtained with cbcroot(V) =',volCbc/(2*rVal.min()),'*2*min(L)')
timing.stats()
print('Avg (1st step excepted) time cost of ILoop is',(plot.data['tILoop'][-1]-plot.data['tILoop'][0])/(O.iter-1)*0.001,'ms/exe') # because PyRunner.initTrue = 0, the 1st iteration is indeed disregard here
plot.plots={'it':'tILoop'}
plot.plot()
