# -*- encoding=utf-8 -*-
# jerome.duriez@inrae.fr
# A single contact between two superquadric ellipsoids described with LevelSet, for the timing analysis of section 5.3

nN = 100 # number of boundary nodes
prec = 10 # grid resolution
from yade import ymport,plot,timing
import time
execfile('ramFP.py')

O.timingEnabled=True

#########################
### Engine definition ###
#########################

O.dt = 1.
nItMax = 150
O.engines=[ForceResetter()
           ,InsertionSortCollider([Bo1_LevelSet_Aabb()],verletDist = 0)
           ,InteractionLoop(
               [Ig2_LevelSet_LevelSet_ScGeom()],
               [Ip2_FrictMat_FrictMat_FrictPhys()],
               [Law2_ScGeom_FrictPhys_CundallStrack(sphericalBodies=False)]
               ,label='iLoop')
           ,NewtonIntegrator()
           ,PyRunner(command='saveThings()',iterPeriod=1)
          ]
def saveThings():
    plot.addData(it = O.iter,tILoop = iLoop.execTime/1000.# tILoop now in us
    )

#########################
### Bodies definition ###
#########################

# Body 0 shall be at the origin, axis-aligned, and the biggest one
O.materials.append(FrictMat())

epsVal = numpy.array([[1.4,1.2] # ~ the ellipsoid
                     ,[0.4,1.6] # ~ the almond
                      ])
rVal = numpy.array([[0.8,1.2,1.6]
                   ,[0.7,1.2,0.9]
                    ])

# some orientation and position for the almond:
iniOri = Quaternion((0.04478760831548503352,0.9377162807771838304,0.344502898256000889),2.58242771884218536)
iniCentr = Vector3(1.092,0.989,0.833)

ramFPini = ramFP()
tStart = time.time()
O.bodies.append(levelSetBody("superellipsoid",extents = rVal[0],epsilons=epsVal[0],spacing = min(rVal[0])/prec,dynamic=False,nNodes=nN,nodesPath=2))
O.bodies.append(levelSetBody("superellipsoid",iniCentr,extents = rVal[1],epsilons=epsVal[1],spacing = min(rVal[1])/prec,orientation=iniOri,dynamic=False,nNodes=nN,nodesPath=2))
tEnd = time.time()
nNtot = sum([len(O.bodies[i].shape.boundNodes) for i in range(2)])
print(len(O.bodies),'ls bodies with a total of',nNtot,'boundary nodes generated in',tEnd-tStart,'s, and',ramFP() - ramFPini,'MB of RAM')

###########
### Run ###
###########

O.run(nItMax,True)
if not O.interactions.has(0,1):
    raise BaseException('PROBLEM: no interaction !')
else:
    if not O.interactions[0,1].isReal:
        raise BaseException('PROBLEM: no real interaction !')
    un = O.interactions[0,1].geom.penetrationDepth
    print('Interaction obtained with un =',un/(2*rVal.min()),'*2*min(L)')
timing.stats()
print('Avg (1st step excepted) time cost of ILoop is',(plot.data['tILoop'][-1]-plot.data['tILoop'][0])/(O.iter-1)*0.001,'ms/exe') # because PyRunner.initTrue = 0, the 1st iteration is indeed disregard here
plot.plots={'it':'tILoop'}
plot.plot()
