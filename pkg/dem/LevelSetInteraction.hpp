/* 
 * Author: jerome.duriez@inrae.fr
 */

#pragma once
#include <pkg/common/Dispatching.hpp>
#include <pkg/dem/LevelSet.hpp>
#include <pkg/common/Box.hpp>
#include <pkg/common/Wall.hpp>
#include <pkg/dem/ScGeom.hpp>

namespace yade {
class Bo1_LevelSet_Aabb : public BoundFunctor{
	public:
	void go(const shared_ptr<Shape>& cm, shared_ptr<Bound>& bv, const Se3r& se3, const Body*);
	
	FUNCTOR1D(LevelSet);
	
	YADE_CLASS_BASE_DOC(Bo1_LevelSet_Aabb,BoundFunctor,"Creates/updates an :yref:`Aabb` of a :yref:`LevelSet`");
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Bo1_LevelSet_Aabb);


class Ig2_LevelSet_LevelSet_ScGeom : public IGeomFunctor
{
	public:
		static shared_ptr<ScGeom> geomPtr(Vector3r ctctPt, Real un, Real rad1, Real rad2, const State& rbp1, const State& rbp2, const Scene* scene, const shared_ptr<Interaction>& c, const Vector3r& currentNormal);
	virtual bool go(const shared_ptr<Shape>&, const shared_ptr<Shape>&,
		 	const State&, const State&,
			const Vector3r&,
			const bool&,
			const shared_ptr<Interaction>&); // reminder: method signature is imposed by InteractionLoop.cpp
	virtual bool goReverse(	const shared_ptr<Shape>&, const shared_ptr<Shape>&,
				const State&, const State&,
				const Vector3r&,
				const bool&,
				const shared_ptr<Interaction>&){ LOG_ERROR("We ended up calling goReverse.. How is this possible for symmetric IgFunctor ? Anyway, we now have to code something");/* nothing, such as in TTetraGeom, mixed examples elsewhere*/ return false;};
		
	YADE_CLASS_BASE_DOC(Ig2_LevelSet_LevelSet_ScGeom,IGeomFunctor,"Creates or updates a :yref:`ScGeom` instance representing the intersection of two :yref:`LevelSet`-shaped bodies. Use in conjunction with a true :yref:`InteractionLoop.warnRoleIGeom` to avoid unnecessary warnings."); // because, contrary to other Ig2, it also has to trigger the erase of the interaction in case the two bodies separate too much.
	DECLARE_LOGGER;
	FUNCTOR2D(LevelSet,LevelSet);
	DEFINE_FUNCTOR_ORDER_2D(LevelSet,LevelSet);
};
REGISTER_SERIALIZABLE(Ig2_LevelSet_LevelSet_ScGeom);

class Ig2_Box_LevelSet_ScGeom : public IGeomFunctor
{

	public:
	virtual bool go(const shared_ptr<Shape>&, const shared_ptr<Shape>&,
			const State&, const State&,
			const Vector3r&,
			const bool&,
			const shared_ptr<Interaction>&);
	virtual bool goReverse(	const shared_ptr<Shape>& cm1, const shared_ptr<Shape>& cm2,
				const State& state1, const State& state2,
				const Vector3r& shift2,
				const bool& force,
				const shared_ptr<Interaction>& c){ c->swapOrder(); return go(cm2,cm1,state2,state1,-shift2,force,c);};
	YADE_CLASS_BASE_DOC(Ig2_Box_LevelSet_ScGeom,IGeomFunctor,"Creates or updates a :yref:`ScGeom` instance representing the intersection of one :yref:`LevelSet` body with one :yref:`Box` body. Restricted to the case of Boxes for which local and global axes coincide, and with non zero thickness, and assuming the center of the level set body never enters into the box (ie excluding big overlaps). Normal is given by the box geometry. You may prefer using :yref:`Ig2_Wall_LevelSet_ScGeom`.");
	DECLARE_LOGGER;
	FUNCTOR2D(Box,LevelSet);
	DEFINE_FUNCTOR_ORDER_2D(Box,LevelSet);
};
REGISTER_SERIALIZABLE(Ig2_Box_LevelSet_ScGeom);

class Ig2_Wall_LevelSet_ScGeom : public IGeomFunctor
{
	public:
	virtual bool go(const shared_ptr<Shape>&, const shared_ptr<Shape>&,
			const State&, const State&,
			const Vector3r&,
			const bool&,
			const shared_ptr<Interaction>&);
	virtual bool goReverse(	const shared_ptr<Shape>& cm1, const shared_ptr<Shape>& cm2,
				const State& state1, const State& state2,
				const Vector3r& shift2,
				const bool& force,
				const shared_ptr<Interaction>& c){ c->swapOrder(); return go(cm2,cm1,state2,state1,-shift2,force,c);};
	YADE_CLASS_BASE_DOC(Ig2_Wall_LevelSet_ScGeom,IGeomFunctor,"Creates or updates a :yref:`ScGeom` instance representing the intersection of one :yref:`LevelSet` body with one :yref:`Wall` body. Overlap is chosen on the opposite wall side than the LevelSet body's center, and contact normal is given by the wall normal.");
	DECLARE_LOGGER;
	FUNCTOR2D(Wall,LevelSet);
	DEFINE_FUNCTOR_ORDER_2D(Wall,LevelSet);
};
REGISTER_SERIALIZABLE(Ig2_Wall_LevelSet_ScGeom);

} // namespace yade

