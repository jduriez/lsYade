// jerome.duriez@inrae.fr

#pragma once

#include <lib/base/Logging.hpp>
#include <lib/base/Math.hpp>
#include <pkg/dem/LevelSet.hpp>

namespace yade { // Cannot have #include directive inside.
class ShopLS {	
public:
	DECLARE_LOGGER;
//	****** Helpful functions for RegularGrid creation *******
//	NB: I could overload in C++ the two following functions but then I would not know how to expose them to Python. Here, with two different C++ names, and one Python name (and two .def in _utils.cpp), it works...
//	See e.g. https://www.boost.org/doc/libs/1_66_0/libs/python/doc/html/tutorial/tutorial/functions.html#tutorial.functions.overloading https://stackoverflow.com/questions/26200998/how-to-wrap-functions-overloaded-by-type for some reading
	static int nGPr(Real,Real,Real); // to have a good number of gridpoints
	static Vector3i nGPv(Vector3r,Vector3r,Real); // "overloaded" (not in C++, will be in Python) version of the above
//	****** Cartesian <-> spherical coordinates functions *******
	static Vector3r cart2spher(Vector3r);
	static Vector3r spher2cart(Vector3r);
//	****** Functions for LS bodies creation. Distances shall always be negative inside *******
	static shared_ptr<LevelSet> lsSimpleShape(int,Vector3r,Real,Vector2r); // will be passed to Python in eg levelSetBody() and "returning shared_ptr<…> objects is the preferred way of passing objects from c++ to python" according to https://yade-dem.org/doc/prog.html#reference-counting
	static Real distToSph(Vector3r, Real rad); // the distance to a sphere surface
	static Real distToCircle(Vector3r, Real rad); // the distance to a circle's contour, in a (x,y) plane
	static Real distToRecParallelepiped(Vector3r pt, Vector3r extents); // the distance to a box
	static Real distApproxSE(Vector3r pt, Vector3r extents, Vector2r epsilons); // the approximated distance function to a superellipsoid
	static Real insideOutsideSE(const Vector3r pt, const Vector3r radii, const Vector2r epsilons); // inside-outside function to a superellipsoid
	static Real distToInterval(Real, Real, Real); // the distance to an interval in 1D space
	static vector<vector<vector<Real>>> distIniSE(Vector3r, Vector2r, shared_ptr<RegularGrid>); // appropriate DistFMM.phiIni for a se distance
	static Real fioRose(Vector3r);
	static Vector3r grad_fioRose(Vector3r);
	static Real distApproxRose(Vector3r);
	static vector<vector<vector<Real>>> phiIniCppPy(shared_ptr<RegularGrid>); // handy function for constructing a Py-related DistFMM.phiIni
//	****** Miscellaneous functions ********
	static Vector3r rigidMapping(Vector3r, const Vector3r, const Vector3r, const Quaternionr); // a rigid body mapping
	static Real biInterpolate(std::array<Real,2>, std::array<Real,2>, std::array<Real,2>, std::array<std::array<Real,2>,2>); // bi-interpolation in a plane, used in distance()
};
} // namespace yade
