# To execute as it is for using LS-DEM in YADE.
# If comparisons with Polyhedra are intended (as done in the manuscript), pay attention to the 2 necessary changes (search for Polyhedra below)

# 1. Prerequisites (Ubuntu 18.04 or 20.04):

sudo apt install cmake git freeglut3-dev libloki-dev libboost-all-dev \
     dpkg-dev build-essential g++ python3-dev python3-ipython python3-matplotlib \
     libsqlite3-dev python3-numpy python3-tk gnuplot libgts-dev python3-pygraphviz \
     libvtk6-dev libeigen3-dev python3-xlib python3-pyqt5 pyqt5-dev-tools python3-mpi4py \
     python3-pyqt5.qtwebkit gtk2-engines-pixbuf python3-pyqt5.qtsvg libqglviewer-dev-qt5 \
     python3-pil libjs-jquery python3-sphinx python3-git libxmu-dev libxi-dev \
     libbz2-dev zlib1g-dev python3-minieigen python3-bibtexparser python3-future #libcgal-dev # uncomment libcgal-dev for Polyhedra comparison
# please also include either paraview-python (Ubuntu 18.04) or python3-paraview (Ubuntu 20.04)

# 2.a Downloading source code:

git clone https://gitlab.com/jduriez/lsYade.git

# 2.b. Backup build of source code (equivalent to 2.a)
# git clone https://gitlab.com/yade-dev/trunk.git
# mv trunk lsYade
# cd lsYade
# git reset --hard 9964f53
# patch -p1 < lsYade.diff # lsYade.diff provided in the .zip "source code" submission item
# git add *
# git commit -a -m 'A LS-DEM Yade version'
# cd ..

# 3. (Possibly parallel) Compilation:

mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=../install -DENABLE_LS=ON -DDEBUG=OFF -DENABLE_ASAN=OFF -DENABLE_POTENTIAL_BLOCKS=OFF -DENABLE_POTENTIAL_PARTICLES=OFF -DENABLE_PFVFLOW=OFF -DENABLE_LBMFLOW=OFF -DENABLE_CGAL=OFF -DENABLE_FEMLIKE=OFF -DSUFFIX=levelSet ../lsYade #  set -DENABLE_CGAL=ON if you wanna make the comparisons with Polyhedra
make install -j 6 # adapt the 6 to desired numbers of threads

#4. Uncomment the following for building local versions (html, ..) of the doc

# make doc

#5. Usage

cd ../install/bin
./yadelevelSet # you can now enjoy the modified YADE platform
